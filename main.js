document.getElementById("rock").addEventListener("click", rockMove);
document.getElementById("paper").addEventListener("click", paperMove);
document.getElementById("scissors").addEventListener("click", scissorsMove);

function rockMove() {
  let player1 = "rock";
  let CPU = CPUmove();``
  document.getElementById("paper").style.visibility = "hidden";
  document.getElementById("scissors").style.visibility = "hidden";

  console.log(player1);
  console.log(CPU);
  console.log(checkTie(player1, CPU));
  console.log(checkWin(player1, CPU));
}

function paperMove() {
  let player1 = "paper";
  let CPU = CPUmove();
  document.getElementById("scissors").style.visibility = "hidden";
  document.getElementById("rock").style.visibility = "hidden";
  console.log(player1);
  console.log(CPU);
  console.log(checkTie(player1, CPU));
  console.log(checkWin(player1, CPU));
}

function scissorsMove() {
  let player1 = "scissors";
  let CPU = CPUmove();
  document.getElementById("paper").style.visibility = "hidden";
  document.getElementById("rock").style.visibility = "hidden";
  console.log(player1);
  console.log(CPU);
  console.log(checkTie(player1, CPU));
  console.log(checkWin(player1, CPU));
}

function CPUmove() {
  let computerMove = Math.random();
  console.log(computerMove);
  let moveResult = "";
  if (computerMove < 0.33) {
    moveResult = "rock";
    document.getElementById("rockCPU").style.visibility = "inherit";
    document.getElementById("refresh").style.visibility = "inherit";
  } else if (computerMove <= 0.67) {
    moveResult = "paper";
    document.getElementById("paperCPU").style.visibility = "inherit";
    document.getElementById("refresh").style.visibility = "inherit";
  } else {
    moveResult = "scissors";
    document.getElementById("scissorsCPU").style.visibility = "inherit";
    document.getElementById("refresh").style.visibility = "inherit";
  }
  return moveResult;
}

function checkTie(player1, CPU) {
  if (player1 === CPU) {
    document.getElementById("winner").innerHTML = "Its a tie, play again!";
  }
}

function checkWin(player1, CPU) {
  if (player1 === "rock") {
    if (CPU === "paper") {
      document.getElementById("winner").innerHTML = "Paper Wins!";
    } else {
      if (CPU === "scissors") {
        document.getElementById("winner").innerHTML = "Rock Wins!";
      }
    }
  }
  if (player1 === "paper") {
    if (CPU === "rock") {
      document.getElementById("winner").innerHTML = "Paper Wins!";
    } else {
      if (CPU === "scissors") {
        document.getElementById("winner").innerHTML = "Scissors Wins!";
      }
    }
  }
  if (player1 === "scissors") {
    if (CPU === "rock") {
      document.getElementById("winner").innerHTML = "Rock Wins!";
    } else {
      if (CPU === "paper") {
        document.getElementById("winner").innerHTML = "Scissors Wins!";
      }
    }
  }
}

// Used Stackover Flow as reference URL is below
// https://stackoverflow.com/questions/17976883/rock-paper-scissors-in-javascript
